<?php
declare(strict_types = 1);
/**
 * A web based (APhp scripting) Monopoly game by Emmanuel Lanuzo
 * Built from scratch. No third party codes
 * Coding using mobile phone (Cherry mobile flare S5 power)
 * Copyright (c) Emmanuel Lanuzo
 * Creation date (August 3, 2018)
 * Apps used for testing and coding:
 * - QuickEdit
 * - Dcoder
 * - Server for PHP
 * - HTTP Server powered by Apache
 * - MariaDB Server
 * - ColorPallete
 * - Termux
 */
ob_start();
session_start();
define('PROTEC', TRUE);
define('PAGE_NAME', "Emmanuel Lanuzo's Venture Game!");
define('HTTP', 0);

// login variables

require_once (htmlspecialchars(htmlentities($_SERVER['DOCUMENT_ROOT'])) . '/langs/en.php');

$url = htmlspecialchars(htmlentities($_SERVER['SERVER_NAME']));
require_once ("db.php");

require_once ("functions/header.php");

// sample login | asynchronous php script

function dief()
{

	// todo show error page instead

	echo "Action failed. Seems like a data transfer between your computer, ISP and our server malfunctioned.\nPlease try again.";
	sleep(5);
	header("Expires: $now");
	header("Last-Modified: $now");
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	header($_SERVER['SERVER_PROTOCOL'] . $en['err416'], 416);
	header("Refresh: 1; URL = " . $url);
	exit();
}

$queryBuilder = $conn->createQueryBuilder();

if (isset($_POST["loginform"]))
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$username = "";
		$password = "";

		// if our query doesn't return empty value

		if (isset($_POST["username"]))
		{
			$username = $_POST["username"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		if (isset($_POST["password"]))
		{
			$password = $_POST["password"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		$usernamesame = $queryBuilder->select("$username")->from('users');
		if ($username === $usernamesame)
		{
			$userpassissame = $queryBuilder->select("$username")->from('users')->where("pass = $password");
			if ($password === $userexists)
			{

				// if our query exists, then set ["belongs"] to "emman"

				$belongs = "emman";
				try
				{
					session_start();
					$error = "Session Error Occured. Can't create or start a session";
					throw new Exception($error);
				}

				catch(Exception $e)
				{

					// todo! make sure this won't happen like instead of echoing, call a function instead to keep everything smooth like a jagger

					echo $e->getMessage();
				}

				$_SESSION["gamer"]["belongs"] = $belongs;
				$_SESSION["gamer"]["user"] = $username;
				$_SESSION["gamer"]["pass"] = $password;
				$value = $_SESSION["gamer"];
				if (isset($_POST["rememberme"]))
				{
					setcookie("login", $value, time() + 3600 * 24 * 30 * 6, "/", "", HTTP);
				}
				else
				{
					setcookie("login", $value, "/", "", HTTP);
				}
			}
			else
			{

				// else if the user doesn't exist/wrong credentials or somebody is already logged in using the current user credentials

				require_once ("error.php");

				exit();
			}
		}
	}

	exit();
}

if (isset($_POST["registerform"]))
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$username = "";
		$useremail = "";
		$password = "";
		$rpassword = "";

		// let's always expect something bad will/or happen

		if (isset($_POST["username"]))
		{

			// then set it as

			$username = $_POST["username"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		if (isset($_POST["email"]))
		{

			// then set it as

			$useremail = $_POST["email"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		if (isset($_POST["password"]))
		{

			// then set it as

			$password = $_POST["password"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		if (isset($_POST["rpassword"]))
		{

			// then set it as

			$rpassword = $_POST["rpassword"];
		}
		else
		{

			// do something here
			// todo set a popup error modal

			die(dief());
		}

		if ($password === $rpassword)
		{
			$new = $queryBuilder->insert("$username")->from("users")->values(array(
				'name' => '?',
				'email' => '?',
				'password' => '?'
			))->setParameter(0, $username)->setParameter(1, $useremail)->setParameter(2, $password);
			$new->execute();
			$belongs = "emman";
			try
			{
				session_start();
				$error = "Session Error Occured. Can't create or start a session";
				throw new Exception($error);
			}

			catch(Exception $e)
			{

				// todo! make sure this won't happen like instead of echoing, call a function instead to keep everything smooth like a jagger
				echo $e->getMessage();
			}

			$_SESSION["gamer"]["belongs"] = $belongs;
			$_SESSION["gamer"]["user"] = $username;
			$_SESSION["gamer"]["pass"] = $password;
			$value = $_SESSION["gamer"];
			if (isset($_POST["rememberme"]))
			{
				setcookie("login", $value, time() + 3600 * 24 * 30 * 6, "/", "", HTTP);
			}
			else
			{
				setcookie("login", $value, "/", "", HTTP);
			}

			exit(header("Location: "$_SERVER['SERVER_NAME']));
		}
		else
		{
			require_once ("register.php");

			exit();
		}
	}

	exit();
}

if ($_GET["logout"])
{
	setcookie("login", time() - 2, "/", "", HTTP);

	// let's make sure

	if (!empty($_SESSION))
	{
		session_start();
		$_SESSION = [];
		session_destroy();
	}
	else
	{
		$now = gmdate('D, d M Y H:i:s') . " UTC";
		header("Expires: $now");
		header("Last-Modified: $now");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header($_SERVER['SERVER_PROTOCOL'] . " 308 Permanent Redirect");
		header("Refresh: 1; URL = " . $_SERVER['SERVER_NAME']);
	}

	exit(); //just to make sure that the code will stop executing from here
}

// check if the cookie exists and that said session belongs to emman

if (!empty($_COOKIE["login"]) && $_SESSION["gamer"]["belongs"] === "emman")
{

	// let's make sure that cookies aren't empty or what or $username really exist in the db

	$username = $_SESSION["gamer"]["user"];

	// to make sure that user exists and to avoid data forgery

	$user = $queryBuilder->select("$username")->from('users');
	if ($username === $user->execute())
	{

		// if returns true

		require_once ("game.php");

		exit();
	}
	else
	{
		try
		{
			session_start();

			// destroy the session if it is broken

			setcookie("login", "", time() - 2, "/", "", HTTP);

			// if some session fragments exists

			$_SESSION = ["gamer"];
			session_destroy();
			$error = "";
			throw new Exception($error);
		}

		catch(Exception $e)
		{

			// todo! make sure this won't happen like instead of echoing, call a function instead to keep everything smooth like a jagger
			echo $e->getMessage();
		}

		// then show the form

		require_once ("login.php");

		exit();
	}
}
