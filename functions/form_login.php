<?php
declare(strict_types = 1);
require_once (htmlspecialchars(htmlentities($_SERVER['DOCUMENT_ROOT'])) . '/langs/en.php');

if (!defined('PROTEC'))
{
	header($_SERVER['SERVER_PROTOCOL'] . $en['err404'], 404);
	exit;
}

