<?php
declare(strict_types = 1);
require_once (htmlspecialchars(htmlentities($_SERVER['DOCUMENT_ROOT'])) . '/langs/en.php');

if (!defined('PROTEC'))
{
	header($_SERVER['SERVER_PROTOCOL'] . $en['err404'], 404);
	exit;
}

$url = htmlspecialchars(htmlentities($_SERVER['SERVER_NAME']));
$cyear = date('Y');
$cday = date("D");
$cdaynumber = date("d");
$cmonth = date("M");
$chour = date("H");
$cminute = date("i");
$csecond = date("s");
function login_signin_page_styles() {
echo "<style type=\"text/css\">
html,
body {
height: 100%;
}
body {
display: -ms-flexbox;
display: flex;
-ms-flex-align: center;
align-items: center;
padding-top: 40px;
padding-bottom: 40px;
background-color: #f5f5f5;
}
.form-signin {
width: 100%;
max-width: 330px;
padding: 15px;
margin: auto;
}
.form-signin .checkbox {
font-weight: 400;
}
.form-signin .form-control {
position: relative;
box-sizing: border-box;
height: auto;
padding: 10px;
font-size: 16px;
}
.form-signin .form-control:focus {
z-index: 2;
}
.form-signin input[type=\"text\"] {
margin-bottom: -1px;
border-bottom-right-radius: 0;
border-bottom-left-radius: 0;
}
.form-signin input[type=\"password\"] {
margin-bottom: 10px;
border-top-left-radius: 0;
border-top-right-radius: 0;
}
</style>";
}
$scripts = [
"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js",
"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
];
function page_scripts() {
foreach ($scripts as $script) {
echo "<script type=\"text/javascript\" src=\"{$url}/{$script}\"></script>";
}
}
function page_form($type) {
if ($type === "login") {
form(["class" => "form-signin", "method" => "post"]);
<img class=\"mb-4\" src=\"assets/brand/bootstrap-solid.svg\" alt=\"\" width=\"72\" height=\"72\">
<h1 class=\"h3 mb-3 font-weight-normal\">Please Sign In</h1>
<p class=\"mb-3 text-muted\">username: sample; pass: abcd;</p>
<input type=\"hidden\" id=\"loginform\" value=\"123\" name=\"loginform\">
<label for=\"Email\" class=\"sr-only\">Username</label>
<input type=\"text\" id=\"Username\" class=\"form-control\" placeholder=\"Username\" name=\"username\" pattern=\"[A-Za-z0-9]\" required autofocus>
<label for=\"Password\" class=\"sr-only\">Password</label>
<input type=\"password\" id=\"Password\" class=\"form-control\" placeholder=\"Password\" name=\"password\" required>
<div class=\"checkbox mb-3\">
<label>
<input type=\"checkbox\" name=\"rememberme\" value=\"remember-me\">Remember me</label>
</div>
<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>
p(["class" => "mt-5 mb-3 text-muted", "insert" => "&copy; $cy"]);
</form>";
exit();
} else if ($type === "register") {
form(["class" => "form-signin", "method" => "post"]);
<img class=\"mb-4\" src=\"assets/brand/bootstrap-solid.svg\" alt=\"\" width=\"72\" height=\"72\">
<h1 class=\"h3 mb-3 font-weight-normal\">Register</h1>
<p class=\"mb-3 text-muted\">Register an account to login for Ventura!</p>
<input type=\"hidden\" id=\"registerform\" value=\"123\" name=\"registerform\">
<label for=\"Username\" class=\"sr-only\">Username</label>
<input type=\"text\" id=\"Username\" class=\"form-control\" placeholder=\"Username\" name=\"username\" required autofocus>
<label for=\"Useremail\" class=\"sr-only\">Email</label>
<input type=\"text\" id=\"Useremail\" class=\"form-control\" placeholder=\"Email\" name=\"email\" pattern=\"[A-Za-z0-9]\" required autofocus>
<label for=\"Password\" class=\"sr-only\">Password</label>
<input type=\"password\" id=\"Password\" class=\"form-control\" placeholder=\"Password\" name=\"password\" required>
<label for=\"RPassword\" class=\"sr-only\">Repeat Password</label>
<input type=\"password\" id=\"RPassword\" class=\"form-control\" placeholder=\"Password\" name=\"rpassword\" required>
<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Register!</button>
p(["class" => "mt-5 mb-3 text-muted", "insert" => "&copy; $cy"]);
</form>";
exit()
} else {
die("Can't show login or register form. Some code typo aye?";
}
}