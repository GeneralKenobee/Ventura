<?php declare(strict_types = 1);
namespace Template;

use Lang\En as En;

defined('PROTEC') or header($_SERVER['SERVER_PROTOCOL'] . En\En::errors("404") , 404);exit;

class Html5{
	function comment($cmt){
		print ("<!-- $cmt -->");
	}	
	function doc($ver){
		switch ($ver){
			case "4S":
			print ("<!DOCTYPE HTML PUBLIC \"-/\/W3C/\/DTD HTML 4.01/\/EN\" \"http:/\/www.w3.org/TR/html4/strict.dtd\">");
			break;
			
			case "4T":
			print ("<!DOCTYPE HTML PUBLIC \"-/\/W3C/\/DTD HTML 4.01 Transitional/\/EN\" \"http:/\/www.w3.org/TR/html4/loose.dtd\">");
			break;
			
			case "4F":
			print ("<!DOCTYPE HTML PUBLIC \"-/\/W3C/\/DTD HTML 4.01 Frameset/\/EN\" \"http:/\/www.w3.org/TR/html4/frameset.dtd\">");
			break;
			
			case "1S":
			print ("<!DOCTYPE html PUBLIC \"-/\/W3C/\/DTD XHTML 1.0 Strict/\/EN\" \"http:/\/www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
			break;
			
			case "1T":
			print ("<!DOCTYPE html PUBLIC \"-/\/W3C/\/DTD XHTML 1.0 Transitional/\/EN\" \"http:/\/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			break;
			
			case "1F":
			print ("<!DOCTYPE html PUBLIC \"-/\/W3C/\/DTD XHTML 1.0 Frameset/\/EN\" \"http:/\/www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">");
			break;
			
			case "1":
			print ("<!DOCTYPE html PUBLIC \"-/\/W3C/\/DTD XHTML 1.1/\/EN\" \"http:/\/www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
			break;
			
			default:
			print ("<!DOCTYPE html>");
			break;
		}
	}
	function a($input){
		print ("<a");
		if (isset($input["download"])){
			print (" download=\"" . $input["download"] . "\"");
		}
		if (isset($input["href"])){
			print (" href=\"" . $input["href"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["media"])){
			print (" media=\"" . $input["media"] . "\"");
		}
		if (isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["ping"])){
			print (" ping=\"" . $input["ping"] . "\"");
		}
		if (isset($input["rel"])){
			print (" rel=\"" . $input["rel"] . "\"");
		}
		if (isset($input["target"])){
			print (" target=\"" . $input["target"] . "\"");
		}
		if (isset($input["type"])){
			print (" type=\"" . $input["type"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</a>");
	}
	function abbr($input){
		if (isset($input["insert"])){
			print ("<abbr");
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}
			print (">");
			if (isset($input["insert"])){
				print ("</abbr>");
			}		}		else{
			echo "<p>No data for &lt;abbr&gt;<p>";
		}
	}
	function address($input){

		// TODO! finish this

		print ("<address");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</address>");
	}
	function area($input){

		// TODO! finish this

		print ("<area");
		if (isset($input["alt"])){
			if (isset($input["href"])){
				print (" alt=\"" . $input["alt"] . "\"");
			}		}
		if (isset($input["coords"])){
			print (" coords=\"" . $input["coords"] . "\"");
		}
		if (isset($input["download"])){
			print (" download=\"" . $input["download"] . "\"");
		}
		if (isset($input["href"])){
			print (" href=\"" . $input["href"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["media"])){
			print (" media=\"" . $input["media"] . "\"");
		}
		if (isset($input["rel"])){
			print (" rel=\"" . $input["rel"] . "\"");
		}
		if (isset($input["shape"])){
			print (" shape=\"" . $input["shape"] . "\"");
		}
		if (isset($input["target"])){
			print (" target=\"" . $input["target"] . "\"");
		}
		if (isset($input["type"])){
			print (" type=\"" . $input["type"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</area>");
	}
	function article($input){

		// TODO! finish this

		print ("<article");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</article>");
	}
	function aside($input){

		// TODO! finish this

		print ("<aside");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</aside>");
	}
	function audio($input){
		print ("<audio");
		if (isset($input["auto"])){
			print (" autoplay=\"" . $input["auto"] . "\"");
		}
		if (isset($input["cons"])){
			print (" controls=\"" . $input["cons"] . "\"");
		}
		if (isset($input["loop"])){
			print (" loop=\"" . $input["loop"] . "\"");
		}
		if (isset($input["mute"])){
			print (" muted=\"" . $input["mute"] . "\"");
		}
		if (isset($input["pre"])){
			print (" preload=\"" . $input["pre"] . "\"");
		}
		if (isset($input["src"])){
			print (" src=\"" . $input["src"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["about"])){
			print ($input["about"]);
		}
		print ("</audio>");
	}
	function b($input){

		// TODO! finish this

		print ("<b");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["href"])){
			print ($input["href"]);
		}
		print ("</b>");
	}
	function base($input){
		print ("<base");
		if (isset($input["href"])){
			print (" href=\"" . $input["href"] . "\"");
		}
		if (isset($input["target"])){
			print (" target=\"" . $input["target"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</base>");
	}
	function bdi($input){

		// TODO! finish this

		print ("<bdi");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</bdi>");
	}
	function bdo($input){
		print ("<bdo");
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</bdo>");
	}
	function blockquote($input){
		print ("<blockquote");
		if (isset($input["cite"])){
			print (" cite=\"" . $input["cite"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</blockquote>");
	}	function body($input){
		switch ($input){
			case "start":
			print ("<body");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			print (">");
			break;
			
			case "end":
			print ("</body>");
			break;
		}
	}
	function br(){
		print ("<br");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
	}
	function button($input){
		print ("<button");
		if (isset($input["focus"])){
			print (" autofocus=\"" . $input["focus"] . "\"");
		}
		if (isset($input["dis"])){
			print (" disabled=\"" . $input["dis"] . "\"");
		}
		if (isset($input["form"])){
			print (" form=\"" . $input["form"] . "\"");
		}
		if (isset($input["action"])){
			print (" formaction=\"" . $input["action"] . "\"");
		}
		if (isset($input["enctype"])){
			print (" formenctype=\"" . $input["enctype"] . "\"");
		}
		if (isset($input["method"])){
			print (" formmethod=\"" . $input["method"] . "\"");
		}
		if (isset($input["noval"])){
			print (" formnovalidate=\"" . $input["noval"] . "\"");
		}
		if (isset($input["target"])){
			print (" formtarget=\"" . $input["target"] . "\"");
		}
		if (isset($input["name"])){
			print (" name=\"" . $input["name"] . "\"");
		}
		if (isset($input["type"])){
			print (" type=\"" . $input["type"] . "\"");
		}
		if (isset($input["value"])){
			print (" value=\"" . $input["value"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</button>");
	}
	function canvas($input){
		print ("<canvas");
		if (isset($input["wid"])){
			print (" width=\"" . $input["wid"] . "\"");
		}
		if (isset($input["hie"])){
			print (" hieght=\"" . $input["hie"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</canvas>");
	}
	function caption($input){
		print ("<caption");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</caption>");
	}
	function cite($input){
		print ("<cite");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</cite>");
	}
	function code($input){
		print ("<code");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</code>");
	}
	function col($input){
		print ("<col");
		if (isset($input["spn"])){
			print (" span=\"" . $input["spn"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</col>");
	}
	function colgroup($input){
		print ("<colgroup");
		if (isset($input["spn"])){
			print (" span=\"" . $input["spn"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</colgroup>");
	}
	function data($input){
		print ("<data");
		if (isset($input["vl"])){
			print (" value=\"" . $input["vl"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</data>");
	}
	function datalist($input){
		print ("<datalist");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</datalist>");
	}
	function dd($input){
		print ("<dd");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</dd>");
	}
	function del($input){
		print ("<del");
		if (isset($input["ct"])){
			print (" cite=\"" . $input["ct"] . "\"");
		}
		if (isset($input["dt"])){
			print (" datetime=\"" . $input["dt"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</del>");
	}
	function details($input){
		print ("<details");
		if (isset($input["opn"])){
			print (" open=\"" . $input["opn"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</details>");
	}
	function dfn($input){
		print ("<dfn");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</dfn>");
	}
	function dialog($input){
		print ("<dialog");
		if (isset($input["opn"])){
			print (" open=\"" . $input["opn"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</dialog>");
	}
	function div($input){
		print ("<div");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</div>");
	}
	function dl($input){
		print ("<dl");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</dl>");
	}
	function dt($input){
		print ("<dt");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</dt>");
	}
	function em($input){
		print ("<em");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</em>");
	}
	function embed($input){
		print ("<embed");
		if (isset($input["ht"])){
			print (" height=\"" . $input["ht"] . "\"");
		}
		if (isset($input["src"])){
			print (" src=\"" . $input["src"] . "\"");
		}
		if (isset($input["typ"])){
			print (" type=\"" . $input["typ"] . "\"");
		}
		if (isset($input["href"])){
			print (" width=\"" . $input["wid"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</embed>");
	}
	function fieldset($input){
		print ("<fieldset");
		if (isset($input["dis"])){
			print (" disabled=\"" . $input["dis"] . "\"");
			if (isset($input["form"])){
				print (" form=\"" . $input["form"] . "\"");
			}
			if (isset($input["name"])){
				print (" name=\"" . $input["name"] . "\"");
			}		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</fieldset>");
	}
	function figcaption($input){
		print ("<figcaption");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</figcaption>");
	}
	function figure($input){
		print ("<figure");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</figure>");
	}
	function footer($input){
		print ("<footer");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</footer>");
	}
	function form($input){
		print ("<form");
		if (isset($input["accp"])){
			print (" accept=\"" . $input["accp"] . "\"");
		}
		if (isset($input["ach"])){
			print (" accept-charset=\"" . $input["ach"] . "\"");
		}
		if (isset($input["action"])){
			print (" action=\"" . $input["action"] . "\"");
		}
		if (isset($input["autocomplete"])){
			print (" autocomplete=\"" . $input["autocomplete"] . "\"");
		}
		if (isset($input["enctype"])){
			print (" enctype=\"" . $input["enctype"] . "\"");
		}
		if (isset($input["method"])){
			print (" method=\"" . $input["method"] . "\"");
		}
		if (isset($input["name"])){
			print (" name=\"" . $input["name"] . "\"");
		}
		if (isset($input["novalidate"])){
			print (" novalidate=\"" . $input["novalidate"] . "\"");
		}
		if (isset($input["target"])){
			print (" target=\"" . $input["target"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</form>");
	}	
	function h($input){
		switch ($input["h"]){
			case "1":
			print ("<h1");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])
				{
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h1>");
			break;
			
			case "2":
			print ("<h2");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h2>");
			break;
			
			case "3":
			print ("<h3");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h3>");
			break;
			
			case "4":
			print ("<h4");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h4>");
			break;
			
			case "5":
			print ("<h5");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
				print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h5>");
			break;
			
			case "6":
			print ("<h6");
			
			if (isset($input["event"])){
				print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
			}			
			if (isset($input["accesskey"])){
				print (" accesskey=\"" . $input["accesskey"] . "\"");
			}			
			if (isset($input["class"])){
				print (" class=\"" . $input["class"] . "\"");
			}			
			if (isset($input["data"])){
				print (" data-=\"" . $input["data"] . "\"");
			}			
			if (isset($input["dir"])){
				print (" dir=\"" . $input["dir"] . "\"");
			}			
			if (isset($input["draggable"])){
				print (" draggable=\"" . $input["draggable"] . "\"");
			}			
			if (isset($input["dropzone"])){
				print (" dropzone=\"" . $input["dropzone"] . "\"");
			}			
			if (isset($input["hidden"])){
				print (" hidden=\"" . $input["hidden"] . "\"");
			}			
			if isset($input["id"])){
				print (" id=\"" . $input["id"] . "\"");
			}			
			if (isset($input["hreflang"])){
				print (" hreflang=\"" . $input["hreflang"] . "\"");
			}			
			if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
			}			
			if (isset($input["style"])){
				print (" style=\"" . $input["style"] . "\"");
			}			
			if (isset($input["tabindex"])){
				print (" tabindex=\"" . $input["tabindex"] . "\"");
			}			
			if (isset($input["title"])){
				print (" title=\"" . $input["title"] . "\"");
			}			
			if (isset($input["translate"])){
				print (" translate=\"" . $input["translate"] . "\"");
			}			
			print (">");
			
			if (isset($input["insert"])){
				print (is($input["insert"]));
			}			
			print ("</h6>");
			break;
		}
	}	
	function head($input){
		print ("<head");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</head>");
	}
	function header($input){
		print ("<header");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</header>");
	}
	function hr($input){
		print ("<hr");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</hr>");
	}	
	function html($input){
		switch ($input){
			case "start":
			print ("<html");
			
			if (isset($input["xml"])){
				print (" xmlns=\"" . $input["xml"] . "\"");
			}			
			print (">");
			break;
			
			case "end":
			print ("</html>");
			break;
		}
	}	
	function i($input){
		print ("<i");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</i>");
	}
	function iframe($input){
		print ("<iframe");
		if (isset($input["ht"])){
			print (" height=\"" . $input["ht"] . "\"");
		}
		if (isset($input["name"])){
			print (" name=\"" . $input["name"] . "\"");
		}
		if (isset($input["sdb"])){
			print (" sandbox=\"" . $input["sdb"] . "\"");
		}
		if (isset($input["src"])){
			print (" src=\"" . $input["src"] . "\"");
		}
		if (isset($input["srcd"])){
			print (" srcdoc=\"" . $input["srcd"] . "\"");
		}
		if (isset($input["wd"])){
			print (" width=\"" . $input["wd"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</iframe>");
	}
	function img($input){
		print ("<img");
		if (isset($input["alt"])){
			print (" alt=\"" . $input["alt"] . "\"");
		}
		if (isset($input["href"])){
			print (" crossorigin=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" height=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" ismap=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" longdesc=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" sizes=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" srcset=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" usemap=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" width=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print ("/>");
	}
	function input($input){
		print ("<input");
		if (isset($input["href"])){
			print (" accept=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" alt=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" autocomplete=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" autofocus=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" checked=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" dirname=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" disabled=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" formaction=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" formenctype=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" formmethod=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" formnovalidate=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" formtarget=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" height=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" list=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" max=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" maxlength=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" min=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" multiple=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" pattern=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" placeholder=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" readonly=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" required=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" size=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" step=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" width=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</input>");
	}
	function ins($input){
		print ("<ins");
		if (isset($input["href"])){
			print (" cite=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" datetime=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</ins>");
	}
	function kbd($input){
		print ("<kbd");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</kbd>");
	}
	function label($input){
		print ("<label");
		if (isset($input["href"])){
			print (" for=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</label>");
	}
	function legend($input){
		print ("<legend");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</legend>");
	}
	function li($input){
		print ("<li");
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</li>");
	}
	function link($input){
		print ("<link");
		if (isset($input["href"])){
			print (" crossorigin=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" href=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" hreflang=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" media=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" rel=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" sizes=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</link>");
	}
	function main($input){
		print ("<main");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</main>");
	}
	function map($input){
		print ("<map");
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</map>");
	}
	function mark($input){
		print ("<mark");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</mark>");
	}
	function meta($input){
		print ("<meta");
		if (isset($input["href"])){
			print (" charset=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" content=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" http-equiv=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		print (">");
	}
	function meter($input){
		print ("<meter");
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" high=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" low=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" max=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" min=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" optimum=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</meter>");
	}
	function nav($input){
		print ("<nav");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</nav>");
	}
	function noscript($input){
		print ("<noscript");
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</noscript>");
	}
	function object($input){
		print ("<object");
		if (isset($input["href"])){
			print (" data=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" declare=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" height=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" usemap=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" width=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</object>");
	}
	function ol($input){
		print ("<ol");
		if (isset($input["href"])){
			print (" reversed=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" start=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</ol>");
	}
	function optgroup($input){
		print ("<optgroup");
		if (isset($input["href"])){
			print (" disabled=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" label=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</optgroup>");
	}
	function option($input){
		print ("<option");
		if (isset($input["href"])){
			print (" disabled=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" label=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" selected=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</option>");
	}
	function output($input){
		print ("<output");
		if (isset($input["href"])){
			print (" for=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</output>");
	}
	function p($input){
		print ("<p");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print ($input["insert"]);
		}
		print ("</p>");
	}
	function param($input){
		print ("<param");
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</param>");
	}
	function picture($input){
		print ("<picture");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</picture>");
	}
	function pre($input){
		print ("<pre");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</pre>");
	}
	function progress($input){
		print ("<progress");
		if (isset($input["href"])){
			print (" max=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" value=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</progress>");
	}
	function q($input){
		print ("<q");
		if (isset($input["href"])){
			print (" cite=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</q>");
	}
	function rp($input){
		print ("<rp");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</rp>");
	}
	function rt($input){
		print ("<rt");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</rt>");
	}
	function ruby($input){
		print ("<ruby");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</ruby>");
	}
	function s($input){
		print ("<s");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</s>");
	}
	function samp($input){
		print ("<samp");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</samp>");
	}
	function script($input){
		print ("<script");
		if (isset($input["href"])){
			print (" async=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" charset=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" defer=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</script>");
	}
	function section($input){
		print ("<section");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</section>");
	}
	function select($input){
		print ("<select");
		if (isset($input["href"])){
			print (" autofocus=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" disabled=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" multiple=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" require=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" size=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</select>");
	}
	function small($input){
		print ("<small");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</small>");
	}
	function source($input){
		print ("<source");
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" srcset=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" media=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" sizes=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</source>");
	}
	function span($input){
		print ("<span");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</span>");
	}
	function strong($input){
		print ("<strong");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</strong>");
	}
	function style($input){
		print ("<style");
		if (isset($input["href"])){
			print (" media=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" type=\"" . $input["href"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</style>");
	}
	function sub($input){
		print ("<sub");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</sub>");
	}
	function summary($input){
		print ("<summary");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</summary>");
	}
	function sup($input){
		print ("<sup");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</sup>");
	}
	function svg($input){
		print ("<svg");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</svg>");
	}
	function table($input){
		print ("<table");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</table>");
	}
	function tbody($input){
		print ("<tbody");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</tbody>");
	}
	function td($input){
		print ("<td");
		if (isset($input["href"])){
			print (" colspan=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" headers=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" rowspan=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</td>");
	}
	function template($input){
		print ("<template");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</template>");
	}
	function textarea($input){
		print ("<textarea");
		if (isset($input["href"])){
			print (" autofocus=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" cols=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" dirname=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" disabled=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" form=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" maxlength=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" name=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" placeholder=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" readonly=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" required=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" rows=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" wrap=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</textarea>");
	}
	function tfoot($input){
		print ("<tfoot");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</tfoot>");
	}
	function th($input){
		print ("<th");
		if (isset($input["href"])){
			print (" abbr=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" colspan=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" headers=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" rowspan=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" scope=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" sorted=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</th>");
	}
	function thead($input){
		print ("<thead");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</thead>");
	}
	function time($input){
		print ("<time");
		if (isset($input["href"])){
			print (" datetime=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</time>");
	}
	function title($input){
		print ("<title");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</title>");
	}
	function tr($input){
		print ("<tr");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</tr>");
	}
	function track($input){
		print ("<track");
		if (isset($input["href"])){
			print (" dafault=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" kind=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" label=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" srclang=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</track>");
	}
	function u($input){
		print ("<u");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</u>");
	}
	function ul($input){
		print ("<ul");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</ul>");
	}
	function var ($input){
		print ("<var");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</var>");
	}
	function video($input){
		print ("<video");
		if (isset($input["href"])){
			print (" autoplay=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" controls=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" height=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" loop=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" muted=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" poster=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" preload=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" src=\"" . $input["href"] . "\"");
		}
		if (isset($input["href"])){
			print (" width=\"" . $input["href"] . "\"");
		}
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</video>");
	}
	function wbr($input){
		print ("<wbr");
		if (isset($input["event"])){
			print (" on" . $input["event"] . "=\"" . $input["script"] . "\"");
		}
		if (isset($input["accesskey"])){
			print (" accesskey=\"" . $input["accesskey"] . "\"");
		}
		if (isset($input["class"])){
			print (" class=\"" . $input["class"] . "\"");
		}
		if (isset($input["data"])){
			print (" data-=\"" . $input["data"] . "\"");
		}
		if (isset($input["dir"])){
			print (" dir=\"" . $input["dir"] . "\"");
		}
		if (isset($input["draggable"])){
			print (" draggable=\"" . $input["draggable"] . "\"");
		}
		if (isset($input["dropzone"])){
			print (" dropzone=\"" . $input["dropzone"] . "\"");
		}
		if (isset($input["hidden"])){
			print (" hidden=\"" . $input["hidden"] . "\"");
		}
		if isset($input["id"])){
			print (" id=\"" . $input["id"] . "\"");
		}
		if (isset($input["hreflang"])){
			print (" hreflang=\"" . $input["hreflang"] . "\"");
		}
		if (isset($input["spellcheck"])){
			print (" spellcheck=\"" . $input["spellcheck"] . "\"");
		}
		if (isset($input["style"])){
			print (" style=\"" . $input["style"] . "\"");
		}
		if (isset($input["tabindex"])){
			print (" tabindex=\"" . $input["tabindex"] . "\"");
		}
		if (isset($input["title"])){
			print (" title=\"" . $input["title"] . "\"");
		}
		if (isset($input["translate"])){
			print (" translate=\"" . $input["translate"] . "\"");
		}
		print (">");
		if (isset($input["insert"])){
			print (is($input["insert"]));
		}
		print ("</wbr>");
	}
}