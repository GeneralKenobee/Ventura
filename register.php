<?php
declare(strict_types = 1);
require_once (htmlspecialchars(htmlentities($_SERVER['DOCUMENT_ROOT'])) . '/langs/en.php');

if (!defined('PROTEC'))
{
	header($_SERVER['SERVER_PROTOCOL'] . $en['err404'], 404);
	exit;
}

$stylesheets = [
"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
];
$url = htmlspecialchars(htmlentities($_SERVER['SERVER_NAME']));
$page_name = PAGE_NAME;
echo <<<EOT
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="$url/fav.png">
<meta name="author" content="@EmmanuelLanuzo / @GeneralKenobee">
<meta name="description" content="Emmanuel Lanuzo's venture game where you might loose your virginity!">
<meta name="keywords" content="Emman, Emmanuel, Lanuzo, GeneralKenobee, Portfolio, Freelancer, Full-time, Part-time, Programmer, Developer, Full-Stack Web Developer, Android Developer, IOS Developer, App Developer">
<title>$page_name</title>
EOT;
foreach ($stylesheets as $sheets) {
echo "<link rel=\"stylesheet\" href=\"{$sheets}\">";
}
echo login_signin_page_styles();
</head>
<body class="text-center">
eco page_form("register");
echo page_scripts();
</body>
</html>